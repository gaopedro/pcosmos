/****************************************************************
        kernel全局数据结构头文件krlglobal_t.h
*****************************************************************
                彭东
****************************************************************/
#ifndef _KRLGLOBAL_T_H
#define _KRLGLOBAL_T_H

// ELF 文件中的数据段 .data
#define KRL_DEFGLOB_VARIABLE(vartype, varname) \
        KEXTERN __attribute__((section(".data"))) vartype varname

#endif // KRLGLOBAL_T_H
