/****************************************************************
        Cosmos kernel全局初始化文件krlinit.c
*****************************************************************
                彭东 
****************************************************************/
#include "cosmostypes.h"
#include "cosmosmctrl.h"

// 入口文件
void init_krl()
{
    init_krlmm();      // 内存初始化
    init_krldevice();  // 设备初始化
    init_krldriver();  // 驱动初始化
    init_krlsched();   // 调度初始化
    init_krlcpuidle(); // 常驻CPU进程初始化
    //STI();
    die(0); // 死循环，不让内核退出
    return;
}
