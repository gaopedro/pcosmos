/**********************************************************
        内核层内存管理文件krlmm.c
***********************************************************
                彭东
**********************************************************/
#include "cosmostypes.h"
#include "cosmosmctrl.h"

// 内存初始化
void init_krlmm()
{
    init_krlpagempol(); // 内存页池初始化
    init_kvirmemadrs(); // 虚拟内存初始化
    return;
}

// 内存分配
// @mmsize 内存大小
adr_t krlnew(size_t mmsize)
{
    if (mmsize == MALCSZ_MIN || mmsize > MALCSZ_MAX)
    {
        return NULL;
    }
    return kmempool_new(mmsize);
}

// 内存释放
// @fradr 内存地址
// @frsz 内存大小
bool_t krldelete(adr_t fradr, size_t frsz)
{
    if (fradr == NULL || frsz == MALCSZ_MIN || frsz > MALCSZ_MAX)
    {
        return FALSE;
    }
    return kmempool_delete(fradr, frsz);
}
